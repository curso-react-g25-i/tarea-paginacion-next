import React, { useContext, useEffect, useState } from 'react';
import Layout from '../Layout';
import Loader from '../components/Loader';
import CardCurso from '../components/CardCurso';

import CursosContext from '../context/CursosContext';
import { useRouter } from 'next/router';

const Bucar = () => {
  const [resultado, setResultado] = useState([]);
  const router = useRouter();
  const {
    query: { q },
  } = router;
  const {
    cursos,
    loadingGetCursos,
    errorGetCursos,
    obtenerCursosContext,
  } = useContext(CursosContext);

  useEffect(() => {
    if (!errorGetCursos) {
      obtenerCursosContext();
    }
  }, []);

  useEffect(() => {
    if (cursos && cursos.length > 0) {
      const busqueda = q.toLowerCase();
      console.log(cursos);
      const filtro = cursos.filter((curso) => {
        return (
          curso.nombre.toLowerCase().includes(busqueda) ||
          curso.descripcion.toLowerCase().includes(busqueda)
        );
      });
      setResultado(filtro);
      console.log(filtro);
    }
  }, [q, cursos]);

  if (!cursos) return <Loader />;

  return (
    <div>
      <Layout>
        <div className='row mt-4'>
          <>
            {resultado.length > 0 ? (
              resultado.map((curso) => (
                <CardCurso key={curso.id} curso={curso} />
              ))
            ) : (
              <div className=' alert alert-warning m-auto'>
                Por el momento no tenemos cursos disponibles
              </div>
            )}
          </>
        </div>
      </Layout>
    </div>
  );
};

export default Bucar;
