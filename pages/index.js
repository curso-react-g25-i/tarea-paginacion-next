import { useContext, useEffect } from 'react';
import Head from 'next/head';
import Layout from '../Layout';
import Loader from '../components/Loader';
import CardCurso from '../components/CardCurso';

import CursosContext from '../context/CursosContext';
import Pagination from '../components/Pagination';
import { normalizeRouteRegex } from 'next/dist/lib/load-custom-routes';

export default function Home() {
  const {
    cursos,
    loadingGetCursos,
    errorGetCursos,
    obtenerCursosContext,
    currentPage, 
    pageCursos,
    nextPageContext,
    previousPageContext
  } = useContext(CursosContext);

  useEffect(() => {
    if (!errorGetCursos) {
      obtenerCursosContext();
    }
  }, []);

  useEffect(() => {
    console.log("iniciando pagiunacion");
  }, [cursos]);

  if (!cursos) return <Loader />;

  return (
    <div>
      <Head>
        <title>Cursos</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Layout>
        <div className='row mt-4'>
          {pageCursos.map((curso) => (
            <CardCurso key={curso.id} curso={curso}/>
          ))}
        </div>
        <Pagination previousPage = {previousPageContext} nextPage = {normalizeRouteRegex} currentPage = {currentPage} />
      </Layout>
    </div>
  );
}
