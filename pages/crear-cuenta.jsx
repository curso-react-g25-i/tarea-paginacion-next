import React, { useContext, useState } from 'react';
import Layout from '../Layout';
import Router from 'next/router';
import { validarCrearCuenta } from '../utils/validaciones';
import useValidacion from '../hooks/useValidacion';

import { FirebaseContext } from '../firebase';

const initialState = {
  nombre: '',
  email: '',
  password: '',
};

const CrearCuenta = () => {
  const [
    valores,
    errores,
    handleChange,
    handleSubmit,
  ] = useValidacion(initialState, validarCrearCuenta, () => crearcuenta());

  const [procesar, setProcesar] = useState(false);
  const [error, setError] = useState('');

  const { firebase } = useContext(FirebaseContext);

  const crearcuenta = async () => {
    const { nombre, email, password } = valores;
    setProcesar(true);
    try {
      await firebase.crear(nombre, email, password);
      Router.push('/');
      setProcesar(false);
    } catch (error) {
      console.log('error');
      console.log(error);
      setProcesar(false);
      setError(error.message);
    }
  };
  return (
    <Layout>
      <h1 className='text-center'>crear cuenta</h1>
      <form
        onSubmit={handleSubmit}
        noValidate={true}
        style={{ maxWidth: '30em', margin: '0px auto' }}
      >
        <div className='form-group'>
          <label htmlFor='exampleInputEmail1'>Nombre</label>
          <input
            type='text'
            className='form-control'
            name='nombre'
            onChange={handleChange}
          />
          {errores.nombre && (
            <span style={{ fontSize: '14px', color: 'red' }}>
              {' '}
              {errores.nombre}
            </span>
          )}
        </div>
        <div className='form-group'>
          <label htmlFor='exampleInputEmail1'>Correo</label>
          <input
            type='email'
            className='form-control'
            name='email'
            onChange={handleChange}
          />
          {errores.email && (
            <span style={{ fontSize: '14px', color: 'red' }}>
              {' '}
              {errores.email}
            </span>
          )}
        </div>
        <div className='form-group'>
          <label htmlFor='exampleInputPassword1'>Contraseña</label>
          <input
            type='password'
            className='form-control'
            name='password'
            onChange={handleChange}
          />
          {errores.password && (
            <span style={{ fontSize: '14px', color: 'red' }}>
              {' '}
              {errores.password}
            </span>
          )}
        </div>
        {error && <div className='alert alert-danger'>{error}</div>}
        <div className='form-group'>
          <button type='submit' className='btn btn-primary'>
            crear cuenta{' '}
            {procesar && (
              <span className='spinner-border spinner-border-sm'></span>
            )}
          </button>
        </div>
      </form>
    </Layout>
  );
};

export default CrearCuenta;
