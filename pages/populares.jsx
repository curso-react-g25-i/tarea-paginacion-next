import { useContext, useEffect } from 'react';
import Head from 'next/head';
import Layout from '../Layout';
import Loader from '../components/Loader';
import CardCurso from '../components/CardCurso';

import CursosContext from '../context/CursosContext';

export default function Populares() {
  const {
    cursos,
    loadingGetCursos,
    errorGetCursos,
    obtenerCursosContext,
  } = useContext(CursosContext);

  useEffect(() => {
    if (!errorGetCursos) {
      obtenerCursosContext('votos');
    }
  }, []);

  if (!cursos) return <Loader />;

  return (
    <div>
      <Head>
        <title>Cursos</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Layout>
        <div className='row mt-4'>
          {cursos.map((curso) => (
            <CardCurso key={curso.id} curso={curso} />
          ))}
        </div>
      </Layout>
    </div>
  );
}
