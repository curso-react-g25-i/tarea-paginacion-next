import React, { useState, useContext } from "react";
import CursosContext from '../../context/CursosContext';

const Pagination = () => {

   //const [page, setPage] = useState(0);

   const {
      nextPageContext,
      previousPageContext,
      currentPage,
    } = useContext(CursosContext);

   const onPrevious = () => {
      console.log("previous");
      previousPageContext();
   }

   const onNext = () => {
      console.log("next");
      nextPageContext();
   }

   return (
      <div>
      <p>Deje la paginación de 1 en 1, se puede cambiar mediante la variable pageNumber en CursosState</p>
      <div className="d-flex justify-content-center align-items-center m-4">
         <button className="btn btn-primary" onClick={onPrevious}>anterior</button>
         <p className="mx-4 my-0">{currentPage+1}</p>
         <button className="btn btn-primary" onClick={onNext}>siguiente</button>
      </div>
      </div>
   );
};

export default Pagination;