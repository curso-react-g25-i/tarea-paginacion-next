import React, { useReducer } from "react";

import firebase from "../firebase";

import CursosContext from "./CursosContext";
import CursosReducer from "./CursosReducer";

const CursosState = (props) => {
  const initialState = {
    addCursoOk: false,
    loadingAddCurso: false,
    errorAddCurso: null,
    cursos: null,
    loadingGetCursos: false,
    errorGetCursos: null,
    currentPage: 0,
    pageCursos: [],
  };

  const crearCursoContext = async (curso) => {
    console.log("los datos del curso son");
    console.log(curso);
    dispatch({
      type: "LOADING_ADD_CURSO",
    });
    try {
      console.log("entramos al try");
      await firebase.db.collection("cursos").add(curso);
      dispatch({
        type: "ADD_CURSO_OK",
      });
    } catch (error) {
      console.log("entramos al catch");
      console.log(error);
      dispatch({
        type: "ADD_CURSO_ERROR",
      });
    }
  };

  const obtenerCursosContext = async (orden = "creado") => {
    dispatch({
      type: "LOADING_GET_CURSOS",
    });
    try {
      const doc = await firebase.db
        .collection("cursos")
        .orderBy(orden, "asc")
        .get();
      const cursos = [];
      doc.forEach((item) => cursos.push({ id: item.id, ...item.data() }));

      getCursosPaginated(cursos, state.currentPage);

      dispatch({
        type: "GET_CURSOS_OK",
        payload: cursos,
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: "GET_CURSOS_ERROR",
      });
    }
  };

  const pageNumber = 1;

  const getCursosPaginated = (allCursos, pageIndex) => {
    const pageCursos = [];
    allCursos.map((c,i) => {
      if(i >= pageNumber*pageIndex && i< pageNumber*(pageIndex+1)){
        pageCursos.push(c);
      }
    });

    dispatch({
      type: "CHANGE_PAGE",
      payload: [pageIndex, pageCursos],
    });

  }

  const nextPageContext = () => {
    const { cursos, currentPage } = state;
    const maxPages = Math.ceil(cursos.length / pageNumber) - 1;

    const next = currentPage < maxPages ? (currentPage+1) : currentPage;

    getCursosPaginated(cursos, next);
  };

  const previousPageContext = () => {
    const { cursos, currentPage } = state;
    const minPage = 0;

    const previous = currentPage > minPage ? (currentPage-1) : minPage;
    getCursosPaginated(cursos, previous);
  };


  const [state, dispatch] = useReducer(CursosReducer, initialState);

  return (
    <CursosContext.Provider
      value={{
        addCursoOk: state.addCursoOk,
        loadingAddCurso: state.loadingAddCurso,
        errorAddCurso: state.errorAddCurso,
        cursos: state.cursos,
        loadingGetCursos: state.loadingGetCursos,
        errorGetCursos: state.errorGetCursos,
        crearCursoContext,
        obtenerCursosContext,
        currentPage: state.currentPage,
        pageCursos: state.pageCursos,
        nextPageContext,
        previousPageContext,
      }}
    >
      {props.children}
    </CursosContext.Provider>
  );
};

export default CursosState;
