export default (state, action) => {
  switch (action.type) {
    case "LOADING_ADD_CURSO":
      return {
        ...state,
        addCursoOk: false,
        loadingAddCurso: true,
        errorAddCurso: null,
      };
    case "ADD_CURSO_OK":
      return {
        ...state,
        addCursoOk: true,
        loadingAddCurso: false,
        errorAddCurso: null,
      };
    case "ADD_CURSO_ERROR":
      return {
        ...state,
        addCursoOk: false,
        loadingAddCurso: false,
        errorAddCurso: "Hubo un error al crear el curso",
      };
    case "LOADING_GET_CURSOS":
      return {
        ...state,
        loadingGetCursos: true,
      };
    case "GET_CURSOS_OK":
      return {
        ...state,
        cursos: action.payload,
        loadingGetCursos: false,
        errorGetCursos: null,
        addCursoOk: false,
      };
    case "GET_CURSOS_ERROR":
      return {
        ...state,
        addCursoOk: false,
        loadingAddCurso: false,
        errorGetCursos: "Hubo un error al obtener los cursos",
      };
    case "CHANGE_PAGE":
      console.log(action.payload[0]);
      return {
        ...state,
        currentPage: action.payload[0],
        pageCursos: action.payload[1]
      };
    default:
      return state;
  }
};
