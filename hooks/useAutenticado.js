import React, { useEffect, useState } from 'react';

import firebase from '../firebase';

const useAutenticado = () => {
  const [usuarioAutenticado, setUsuarioAutenticado] = useState(null);
  const [consultando, setConsultando] = useState(true);

  useEffect(() => {
    const usuarioStorage = JSON.parse(localStorage.getItem('usuarioStorage'));
    if (usuarioStorage) {
      setUsuarioAutenticado(usuarioStorage);
    } else {
      const unsusctibe = firebase.auth.onAuthStateChanged((user) => {
        if (user) {
          setUsuarioAutenticado(user);
        } else {
          setUsuarioAutenticado(null);
        }
        setConsultando(false);
        return () => unsusctibe();
      });
    }
  }, []);

  return [usuarioAutenticado, consultando];
};

export default useAutenticado;
